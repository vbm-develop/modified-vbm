# README #

# How to cite:
Radfar, S.; Kinaoush, B. Developing an Extended Virtual Blade Model for Efficient Numerical Modeling of Wind and Tidal Farms. Preprints 2022, 2022030238 (doi: 10.20944/preprints202203.0238.v1).

### What is this repository for? ###
It has been proved that Virtual Blade Model (VBM) is one most efficient and precise models for the analysis of wind and tidal turbines. This feature has been added to ANSYS FLUENT software since 2006, but is limited for only 10 turbines. The final objective of current project is to enable ANSYS FLUENT to accept any desired number of turbines and increase its applicability in engineering problems.

### How do I get set up? ###
1. Setup Model/s:
According to the physics of the flow field user will select required model/s to simulate the flow.
2. Setup Working Fluid/s & Solid/s:
User will define the physical and thermodynamical properties of the working fluid/s and solid/s in the problem.
3. Setup Boundary & Zone Conditions:
Solving the governing equations of the flow (i.e. system of partial differential equations) requires well-defined boundary conditions within the CFD domain. These conditions are selected and defined in this step.
4. Setup Solution Methods:
In CFD simulations the governing equations of the flow are solve numerically. Based on the physics of the problem appropriate numerical schemes and solution methods are selected at this step.

For more information and step by step guide go to following link:
http://nbviewer.jupyter.org/github/sfo-project/3D-flow-over-WindTidal-turbine-VBM-turbulent/blob/master/Fluent/Simulation.ipynb

### Who do I talk to? ###
The audiences of this repository are the researchers and the engineers of energy sector; especially, in the fields of tidal and wind energies.
